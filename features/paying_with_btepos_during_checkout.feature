@paying_for_order_with_btepos
Feature: Paying with BTePOS during checkout
    In order to buy products
    As a Customer
    I want to be able to pay with BTePOS

    Background:
        Given the store operates on a single channel in "Romania"
        And there is a user "ion@swarmsoft.ro" identified by "pass12345"
        And the store has a payment method "BTePOS" with a code "btepos" and BTePOS Checkout gateway
        And the store has a product "PHP T-Shirt" priced at "19.99 lei"
        And the store ships everywhere for free
        And I am logged in as "ion@swarmsoft.ro"

    @ui
    Scenario: Successful payment
        Given I added product "PHP T-Shirt" to the cart
        And I have proceeded selecting "BTePOS" payment method
        When I confirm my order with BTePOS payment
        And I sign in to BTePOS and pay successfully
        Then I should be notified that my payment has been completed

    @ui
    Scenario: Cancelling the payment
        Given I added product "PHP T-Shirt" to the cart
        And I have proceeded selecting "BTePOS" payment method
        When I confirm my order with BTePOS payment
        And I cancel my BTePOS payment
        I should be able to pay again

    @ui
    Scenario: Retrying the payment with success
        Given I added product "PHP T-Shirt" to the cart
        And I have proceeded selecting "BTePOS" payment method
        And I have confirmed my order with BTePOS payment
        But I have cancelled BTePOS payment
        When I try to pay again with BTePOS payment
        And I sign in to BTePOS and pay successfully
        Then I should be notified that my payment has been completed
        And I should see the thank you page

    @ui
    Scenario: Retrying the payment and failing
        Given I added product "PHP T-Shirt" to the cart
        And I have proceeded selecting "BTePOS" payment method
        And I have confirmed my order with BTePOS payment
        But I have cancelled BTePOS payment
        When I try to pay again with BTePOS payment
        And I cancel my BTePOS payment
        I should be able to pay again
