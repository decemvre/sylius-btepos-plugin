## Overview

The plugin integrates [BTePOS payments](https://btepos.ro/e-commerce) with Sylius based applications. After the installation you should be able to create a payment method for BTePOS gateway and enable its payments in your web store.


## Installation

```bash
$ composer require swarmsoft/sylius-btepos-plugin

```
    
Add plugin dependencies to your config/bundles.php file:

```php
return [
    Swarmsoft\SyliusBTePOSPlugin\SwarmsoftSyliusBTePOSPlugin::class => ['all' => true],
]
```

When creating a new Payment method in your Sylius shop select BTePOS Payment from the Create dropdown and then fill in the configuration form. Choose either 1-Phase or 2-Phase, fill-in Base url with the sandbox or production URL, Username, Password and finally fill in 0 for Installment.

If you've agreed with BTePOS on enabling payment installments with STARCard then fill in one of the agreed upon installments (3, 6, 9, etc). This payment method will then only work with the STARCard credit card.

## Testing
```bash
$ wget http://getcomposer.org/composer.phar
$ php composer.phar install
$ yarn install
$ yarn run gulp
$ php bin/console sylius:install --env test
$ php bin/console server:start --env test
$ open http://localhost:8000
$ bin/behat features/*
$ bin/phpspec run
```

## Contribution

Learn more about our contribution workflow on <http://docs.sylius.org/en/latest/contributing/>.
