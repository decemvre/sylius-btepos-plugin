<?php

declare(strict_types=1);

namespace Swarmsoft\SyliusBTePOSPlugin\Payum;

use Swarmsoft\SyliusBTePOSPlugin\Payum\Action\StatusAction;
use GuzzleHttp\Client;
use Payum\Core\Bridge\Spl\ArrayObject;
use Payum\Core\GatewayFactory;

final class BTePOSPaymentGatewayFactory extends GatewayFactory
{
    protected function populateConfig(ArrayObject $config): void
    {
        $client = new Client();
        $config->defaults([
            'payum.factory_name' => 'btepos_payment',
            'payum.factory_title' => 'BTePOS Payment',
            'payum.action.status' => new StatusAction($client),
        ]);
        $config['payum.api'] = function (ArrayObject $config) {
            return new BTePOSApi(
                    $config['phase'],
                    $config['baseUrl'],
                    $config['userName'],
                    $config['password'],
                    $config['installment']
                );
        };
    }
}
