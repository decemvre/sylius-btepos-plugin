<?php

declare(strict_types=1);

namespace Swarmsoft\SyliusBTePOSPlugin\Payum\Action;

use Swarmsoft\SyliusBTePOSPlugin\Payum\Action\traits\FormParamsAuthTrait;
use Swarmsoft\SyliusBTePOSPlugin\Payum\BTePOSApi;
use Detection\MobileDetect;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Payum\Core\Action\ActionInterface;
use Payum\Core\ApiAwareInterface;
use Payum\Core\Exception\RequestNotSupportedException;
use Payum\Core\Exception\UnsupportedApiException;
use Payum\Core\Reply\HttpRedirect;
use Payum\Core\Request\Capture;
use Sylius\Component\Core\Model\PaymentInterface as SyliusPaymentInterface;
use Payum\ISO4217\ISO4217;

final class CaptureAction implements ActionInterface, ApiAwareInterface
{
    use FormParamsAuthTrait;

    private $client;

    private $iso4217;

    private $mobileDetect;

    private $api;

    public function __construct(Client $client, ISO4217 $iso4217, MobileDetect $mobileDetect)
    {
        $this->client = $client;
        $this->iso4217 = $iso4217;
        $this->mobileDetect = $mobileDetect;
    }

    public function execute($request): void
    {
        RequestNotSupportedException::assertSupports($this, $request);

        $payment = $request->getModel();
        $details = $payment->getDetails();
        if (isset($details['remoteFormUrl'])) {
            // payment was Created in BTePOS previously and abandoned by the user so it can be reused
            throw new HttpRedirect($details['remoteFormUrl']);
        }
        $order = $payment->getOrder();
        $query = $this->buildQueryParams($order, $payment, $request);
        $formParams = $this->buildFormParams();

        try {
            $response = $this->client->request('POST', $this->api->getRegisterPath(), [
                'query' => $query,
                'form_params' => $formParams,
            ]);
        } catch (RequestException $exception) {
            $response = $exception->getResponse();
        } finally {
            // HTTP Status Codes are always 200 at the moment and Content-Type always text/plain
            // so we need to first validate json and then check for orderId and formUrl or errorCode
            $body = json_decode($response->getBody()->getContents(), true);
            if (is_array($body) && isset($body['orderId'], $body['formUrl'])) {
                $details = ['remoteOrderId' => $body['orderId'], 'remoteFormUrl' => $body['formUrl']];
                $payment->setDetails($details);

                throw new HttpRedirect($body['formUrl']);
            }
            if (is_array($body) && isset($body['errorCode'], $body['errorMessage'])) {
                $details = ['errorCode' => $body['errorCode'], 'errorMessage' => $body['errorMessage']];
                $payment->setDetails($details);

                throw new \Exception($body['errorMessage']);
            }
            // Server returned invalid json or no orderId / formUrl and no errorCode
            $details = ['errorCode' => $response->getStatusCode(), 'errorMessage' => 'An unknown server error occured.'];
            $payment->setDetails($details);

            throw new \Exception($attemptDetails['errorMessage']);
        }
    }

    public function supports($request): bool
    {
        return
            $request instanceof Capture &&
            $request->getModel() instanceof SyliusPaymentInterface
        ;
    }

    public function setApi($api): void
    {
        if (!$api instanceof BTePOSApi) {
            throw new UnsupportedApiException('Not supported. Expected an instance of ' . BTePOSApi::class);
        }

        $this->api = $api;
    }

    private function buildQueryParams($order, SyliusPaymentInterface &$payment, $request): array
    {
        $queryParams = [];
        $queryParams['orderNumber'] = $order->getNumber() . '-' . $payment->getId();
        $queryParams['description'] = $order->getNumber();
        $queryParams['amount'] = $payment->getAmount();
        $queryParams['currency'] = $this->iso4217->findByAlpha3($payment->getCurrencyCode())->getNumeric();
        $queryParams['language'] = $this->getISO6391LetterCode($order->getLocaleCode());
        $queryParams['returnUrl'] = $request->getToken()->getAfterUrl();
        $installment = $this->api->getInstallment();
        if ($installment > 0) {
            $queryParams['installment'] = $installment;
        }
        $queryParams['pageView'] = ($this->mobileDetect->isMobile() || $this->mobileDetect->isTabled()) ? 'MOBILE' : 'DESKTOP';

        return $queryParams;
    }

    private function getISO6391LetterCode(string $localeISO15897Code): string
    {
        switch ($localeISO15897Code) {
            case 'ro_RO':
                return 'ro';
                break;
            case 'en_US':
                return 'en';
                break;
            default:
                return 'en';
                break;
        }
    }
}
