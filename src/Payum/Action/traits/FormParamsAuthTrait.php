<?php

declare(strict_types=1);

namespace Swarmsoft\SyliusBTePOSPlugin\Payum\Action\traits;

trait FormParamsAuthTrait
{
    private function buildFormParams(): array
    {
        return [
            'userName' => $this->api->getUserName(),
            'password' => $this->api->getPassword(),
        ];
    }
}
