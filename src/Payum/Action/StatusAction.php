<?php

declare(strict_types=1);

namespace Swarmsoft\SyliusBTePOSPlugin\Payum\Action;

use Swarmsoft\SyliusBTePOSPlugin\Payum\Action\traits\FormParamsAuthTrait;
use Swarmsoft\SyliusBTePOSPlugin\Payum\BTePOSApi;
use GuzzleHttp\Client;
use Payum\Core\Action\ActionInterface;
use Payum\Core\ApiAwareInterface;
use Payum\Core\Exception\RequestNotSupportedException;
use Payum\Core\Exception\UnsupportedApiException;
use Payum\Core\Request\GetStatusInterface;
use Sylius\Component\Core\Model\PaymentInterface as SyliusPaymentInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\PhpBridgeSessionStorage;

final class StatusAction implements ActionInterface, ApiAwareInterface
{
    use FormParamsAuthTrait;

    private $client;

    private $api;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function execute($request): void
    {
        RequestNotSupportedException::assertSupports($this, $request);

        $payment = $request->getFirstModel();
        $order = $payment->getOrder();
        $details = $payment->getDetails();
        $formParams = $this->buildFormParams();

        try {
            $response = $this->client->request('POST', $this->api->getOrderStatusExtendedPath(), [
                'query' => [
                    'orderId' => $details['remoteOrderId'],
                    'orderNumber' => $order->getNumber() . '-' . $payment->getId(),
                ],
                'form_params' => $formParams,
            ]);
        } catch (RequestException $exception) {
            $response = $exception->getResponse();
            $session->getFlashBag()->add('error', 'Unknown error; we could not determine the status of your payment. Please contact a website administrator.');
            $request->markUnknown();
        } finally {
            // HTTP Status Codes are always 200 at the moment and Content-Type always text/plain
            // so we need to first validate json and then check for orderId and formUrl or errorCode
            $responseBody = json_decode($response->getBody()->getContents(), true);
            $this->markPayment($request, $responseBody);
        }
    }

    private function markPayment(&$request, $responseBody): void
    {
        if (is_array($responseBody) && isset($responseBody['orderStatus'])) {
            switch ($responseBody['orderStatus']) {
                case 0:
                    $request->markNew();

                    break;
                case 1:
                    $request->markAuthorized();

                    break;
                case 2:
                    $request->markCaptured();

                    break;
                case 3:
                    $request->markRefunded();

                    break;
                case 4:
                    $request->markRefunded();

                    break;
                case 5:
                    $request->markPending();

                    break;
                case 6:
                    $request->markFailed();

                    break;
                default:
                    $request->markUnknown();

                    break;
            }
        } else {
            // Server returned invalid json or no orderStatus or no errorCode
            $request->markUnknown();
        }
        if (isset($responseBody['actionCode']) && $responseBody['actionCode'] > 0) {
            $session = new Session(new PhpBridgeSessionStorage());
            $session->start();
            $errorMessage = $this->getErrorMessageTranslationFromActionCode($responseBody['actionCode']);
            $session->getFlashBag()->add('error', $errorMessage);
        } elseif (isset($responseBody['errorCode']) && $responseBody['errorCode'] != 0 && isset($responseBody['errorMessage'])) {
            $session = new Session(new PhpBridgeSessionStorage());
            $session->start();
            $session->getFlashBag()->add('error', $errorMessage);
        }
    }

    public function supports($request): bool
    {
        return
            $request instanceof GetStatusInterface &&
            $request->getFirstModel() instanceof SyliusPaymentInterface
        ;
    }

    public function setApi($api): void
    {
        if (!$api instanceof BTePOSApi) {
            throw new UnsupportedApiException('Not supported. Expected an instance of ' . BTePOSApi::class);
        }

        $this->api = $api;
    }

    private function getErrorMessageTranslationFromActionCode($actionCode)
    {
        switch ($actionCode) {
            case 320:
                $errorMessage = 'sylius_btepos_plugin.action_code_320';

                break;
            case 801:
                $errorMessage = 'sylius_btepos_plugin.action_code_801';

                break;
            case 803:
                $errorMessage = 'sylius_btepos_plugin.action_code_803';

                break;
            case 805:
                $errorMessage = 'sylius_btepos_plugin.action_code_805';

                break;
            case 861:
                $errorMessage = 'sylius_btepos_plugin.action_code_861';

                break;
            case 871:
                $errorMessage = 'sylius_btepos_plugin.action_code_871';

                break;
            case 906:
                $errorMessage = 'sylius_btepos_plugin.action_code_906';

                break;
            case 914:
                $errorMessage = 'sylius_btepos_plugin.action_code_914';

                break;
            case 915:
                $errorMessage = 'sylius_btepos_plugin.action_code_915';

                break;
            case 917:
                $errorMessage = 'sylius_btepos_plugin.action_code_917';

                break;
            case 998:
                $errorMessage = 'sylius_btepos_plugin.action_code_998';

                break;
            default:
                $errorMessage = 'sylius_btepos_plugin.action_code_def';

                break;
        }

        return $errorMessage;
    }
}
