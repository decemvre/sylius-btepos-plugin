<?php

declare(strict_types=1);

namespace Swarmsoft\SyliusBTePOSPlugin\Payum;

final class BTePOSApi
{
    public const RECURRENCE_TYPE_MANUAL = 'MANUAL';

    private static $apis = [
        'registerDoPath' => '/payment/rest/register.do',
        'registerPreAuthDoPath' => '/payment/rest/registerPreAuth.do',
        'depositDoPath' => '/payment/rest/deposit.do',
        'recurringPaymentDoPath' => '/payment/rest/recurringPayment.do',
        'reverseDoPath' => '/payment/rest/reverse.do',
        'refundDoPath' => '/payment/rest/refund.do',
        'getOrderStatusExtendedDoPath' => '/payment/rest/getOrderStatusExtended.do',
    ];

    private $phase;

    private $baseUrl;

    private $userName;

    private $password;

    private $installment;

    // private $recurrenceType;

    // private $recurrenceStartDate;

    // private $recurrenceEndDate;

    public function __construct(
        int $phase,
        string $baseUrl,
        string $userName,
        string $password,
        int $installment
    ) {
        $this->phase = $phase;
        $this->baseUrl = $baseUrl;
        $this->userName = $userName;
        $this->password = $password;
        $this->installment = $installment;
    }

    // private function setRecurrenceParams(int $recurrenceStartInMonthsFromToday, int $recurrenceEndInMonthsFromToday): void
    // {
    //     $recurrenceStartInMonthsFromToday = abs($recurrenceStartInMonthsFromToday);
    //     $recurrenceEndInMonthsFromToday = abs($recurrenceEndInMonthsFromToday);
    //     if ($recurrenceEndInMonthsFromToday > $recurrenceStartInMonthsFromToday) {
    //         $this->recurrenceType = self::RECURRENCE_TYPE_MANUAL;
    //         if ($recurrenceStartInMonthsFromToday > 0) {
    //             // months from today
    //             $dateTime = new DateTime();
    //             $thisDay = $dateTime->format('j');
    //             if ($recurrenceStartInMonthsFromToday === 1) {
    //                 $rsms = 'first day of +'.$recurrenceStartInMonthsFromToday.' month';
    //             } else {
    //                 $rsms = 'first day of +'.$recurrenceStartInMonthsFromToday.' months';
    //             }
    //             $dateTime->modify($rsms)->modify('+'.(min($thisDay, $dt->format('t'))-1).' days');
    //             $this->recurrenceStartDate = $dateTime->format('Ymd');
    //         } else {
    //             // today
    //             $now = new DateTime();
    //             $this->recurrenceStartDate = $now->format('Ymd');
    //         }
    //         // months from today
    //         $dT = new DateTime();
    //         $tD = $dT->format('j');
    //         if ($recurrenceEndInMonthsFromToday === 1) {
    //             $rems = 'first day of +'.$recurrenceEndInMonthsFromToday.' month';
    //         } else {
    //             $rems = 'first day of +'.$recurrenceEndInMonthsFromToday.' months';
    //         }
    //         $dT->modify($rems)->modify('+'.(min($tD, $dt->format('t'))-1).' days');
    //         $this->recurrenceStartDate = $dT->format('Ymd');
    //     }
    // }

    public function getPhase(): int
    {
        return $this->phase;
    }

    public function getBaseUrl(): string
    {
        return $this->baseUrl;
    }

    public function getUserName(): string
    {
        return $this->userName;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function getInstallment(): int
    {
        return $this->installment;
    }

    public function getRegisterPath()
    {
        $phase = $this->getPhase();
        switch ($phase) {
            case 1:
                return $this->getBaseUrl() . self::$apis['registerDoPath'];

                break;
            case 2:
                return $this->getBaseUrl() . self::$apis['registerPreAuthDoPath'];

                break;
            default:
                return $this->getBaseUrl() . self::$apis['registerDoPath'];

                break;
        }
    }

    public function getOrderStatusExtendedPath()
    {
        return $this->getBaseUrl() . self::$apis['getOrderStatusExtendedDoPath'];
    }
}
