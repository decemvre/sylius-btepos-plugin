<?php

declare(strict_types=1);

namespace Swarmsoft\SyliusBTePOSPlugin;

use Sylius\Bundle\CoreBundle\Application\SyliusPluginTrait;
use Symfony\Component\HttpKernel\Bundle\Bundle;

final class SwarmsoftSyliusBTePOSPlugin extends Bundle
{
    use SyliusPluginTrait;
}
