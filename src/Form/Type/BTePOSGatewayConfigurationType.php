<?php

declare(strict_types=1);

namespace Swarmsoft\SyliusBTePOSPlugin\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

final class BTePOSGatewayConfigurationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('phase', ChoiceType::class, [
            'choices' => [
                '1-Phase' => 1,
                '2-Phase' => 2,
            ],
        ]);
        $builder->add('baseUrl', TextType::class);
        $builder->add('userName', TextType::class);
        $builder->add('password', TextType::class);
        $builder->add('installment', IntegerType::class, ['attr' => ['min' => '0', 'max' => '72']]);
    }
}
