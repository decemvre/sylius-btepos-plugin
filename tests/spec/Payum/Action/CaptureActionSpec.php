<?php

declare(strict_types=1);

namespace spec\Swarmsoft\SyliusBTePOSPlugin\Payum\Action;

use Swarmsoft\SyliusBTePOSPlugin\Payum\Action\CaptureAction;
use GuzzleHttp\Client;
use PhpSpec\ObjectBehavior;
use Payum\Core\Request\Capture;
use Iterator;
use Sylius\Component\Core\Model\OrderInterface;
use Sylius\Component\Core\Model\PaymentInterface;
use Sylius\Component\Core\Model\OrderItemInterface;
use Sylius\Component\Core\Model\CustomerInterface;
use Payum\Core\Bridge\Spl\ArrayObject;
use Payum\Core\Exception\RequestNotSupportedException;

final class CaptureActionSpec extends ObjectBehavior
{
    function let(Client $guzzleHttpClient): void
    {
        $this->beConstructedWith($guzzleHttpClient);
    }

    function it_is_initializable(): void
    {
        $this->shouldHaveType(CaptureAction::class);
    }

    function it_executes(
        Capture $request,
        ArrayObject $model,
        Iterator $iterator,
        OrderItemInterface $orderItem,
        OrderInterface $order,
        PaymentInterface $payment,
        CustomerInterface $customer
    ): void {
        $model->getIterator()->willReturn($iterator);
        $request->getModel()->willReturn($payment);
        $request->getFirstModel()->willReturn($orderItem);
        $orderItem->getOrder()->willReturn($order);
        $order->getCustomer()->willReturn($customer);
    }

    function it_throws_exception_when_model_is_not_array_object(Capture $request): void
    {
        $request->getModel()->willReturn(null);
        $this->shouldThrow(RequestNotSupportedException::class)->during('execute', [$request]);
    }
}
